import java.util.LinkedList;
import java.util.StringTokenizer;

public class LongStack {

   private LinkedList<Long> longStack;

   //http://interactivepython.org/runestone/static/pythonds/BasicDS/InfixPrefixandPostfixExpressions.html

   public static void main (String[] argum) {

      //System.out.println(LongStack.interpret (" 5 5 - 2  "));
      //System.out.println(LongStack.interpret (" 58 5 -   "));
      //System.out.println(LongStack.interpret ("   "));
      //System.out.println(LongStack.interpret ("  5 5  "));
      //System.out.println(LongStack.interpret (" 4 3 3 - +"));


   }

   LongStack() {
      longStack = new LinkedList<Long>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {

      LongStack klone = new LongStack();
      klone.longStack = (LinkedList<Long>)longStack.clone();
      return klone;

   }

   public boolean stEmpty() {

      return longStack.isEmpty();

   }

   public void push (long a) {
      longStack.push(a);
   }

   public long pop() {

      if (longStack.isEmpty()){

         throw new RuntimeException("Pole piisavalt elemente");

      }
      return longStack.pop();

   }

   public void op (String s) {
      if(longStack.size() < 2)
      {
         throw new RuntimeException("Ei saa teha tegevust: " + s + " Põhjus: pole piisavalt elemente!");
      }
      long num2 = longStack.pop();
      long num1 = longStack.pop();
      if (s.equals("+")) {
         push(num1 + num2);
      } else if (s.equals("-")) {
         push(num1 - num2);
      } else if (s.equals("*")) {
         push(num1 * num2);
      } else if (s.equals("/")) {
         push(num1 / num2);
      } else if(s.equals(" ") | s.equals("\t")) {
         return;
      } else {
         throw new RuntimeException("Vale tegevus! " + s + " (võimalikud tegevused on: -, +, / ja *)");
      }

   }

   public long tos() {

      if (stEmpty()) {
         throw new IndexOutOfBoundsException("Pole piisavalt numbreid operatsiooni jaoks");
      }

      return longStack.peekFirst();

   }

   @Override
   public boolean equals (Object o) {

      return longStack.equals(((LongStack)o).longStack);
   }

   @Override
   public String toString() {

      StringBuilder stringBuilder = new StringBuilder();

      for (int i = longStack.size()-1; i >=  0; i--) stringBuilder.append(longStack.get(i));
      return stringBuilder.toString();

   }



   public static long interpret (String pol) {


      if (pol == null || pol.length() == 0)
         throw new RuntimeException("Avaldis: " + pol + "ei saa olla tyhi" );

      LongStack longStack = new LongStack();

      StringTokenizer st = new StringTokenizer(pol.trim());
      int i = 1;
      int kogus = st.countTokens();


      while (st.hasMoreTokens())
      {

         String token = st.nextToken();



         if (token.equals("/") || token.equals("+") || token.equals("*") || token.equals("-")) {
            longStack.op(token);


         }
         else
         {
            if (kogus == i && i > 2 || kogus%2 == 0)
               throw new RuntimeException("Vigane avaldis " + pol + " Kontrollige, et numbide arv oleks ühe vorra rohkem kui operaatorite arv. Lubatud operaatorid on: +, *, -, /" );
            try
            {
               long a = Long.parseLong(token);
               longStack.push(a);

            }
            catch(NumberFormatException e)
            {
               throw new RuntimeException("Avaldis: " + pol +  "See sümbol ei sobi:'" + token );
            }

         }
         i++;

      }

      return longStack.tos();
   }

}